from selenium.webdriver.common.keys import Keys
import time


# Страница товара
class CartPage(object):

    def __init__(self, driver):
        self.driver = driver

    # Ищет товар в корзине и возвращает его имя
    def name_product_in_cart(self):
        return self.driver.find_element_by_xpath('//div[@class="cart-list__product-name"]/a').text


# Страница сравнения товаров
class ComparisonPage(object):

    def __init__(self, driver):
        self.driver = driver

    # Возвращает список заголовков товаров в сравнении
    def search_product_in_comparison(self):
        return self.driver.find_elements_by_xpath('//div[@class="item-name"]/a')


# Страница результата поиска
class SearchResultPage(object):

    def __init__(self, driver):
        self.driver = driver

    # Возвращает список заголовков найденых товаров
    def search_result_title(self):
        return self.driver.find_elements_by_xpath('//a[@class="ui-link"]')

    # Ставит Чек на checkbox сравнение на странице найденых товаров
    def search_result_checkbox(self):
        for elem in self.driver.find_elements_by_xpath('//label[@class="ui-checkbox"]/span[@class="ui-checkbox__box"]')[0:2]:
            # Если не ставить задержку, то на вкладке 'Сравнение' не всегда все выбранные товары
            time.sleep(1)
            elem.click()
            time.sleep(1)

    # Добавить элемент под номером в корзину
    def add_to_cart(self, product_id):
        self.driver.find_elements_by_xpath('//div[@class="primary-btn"]/button')[product_id].click()

    # Открытие страницы корзины
    def open_cart(self):
        self.driver.find_element_by_xpath('//a[@class="btn-cart-link"]').click()
        return CartPage(self.driver)


# Главная страница
class MainPage(object):

    def __init__(self, driver):
        self.driver = driver

    # Находит строку поиска и вбивает запрос; возвращает страницу результата
    def search(self, param):
        element = self.driver.find_element_by_name('q')
        element.send_keys(param)
        element.send_keys(Keys.ENTER)
        return SearchResultPage(self.driver)

    # Возвращает открытую страницу 'Сравнение'
    def start_comparison(self):
        # self.driver.get('https://www.dns-shop.ru/catalog/product/compare')
        self.driver.find_element_by_xpath('//i[@class="header-menu-btn navbar-toggle collapsed has-notifies"]').click()
        time.sleep(1)
        self.driver.find_element_by_xpath('//ul/li[@class="hide menu-item-compare"]/a').click()
        return ComparisonPage(self.driver)

    # Открытие страницы корзины
    def open_cart(self):
        self.driver.find_element_by_xpath('//a[@class="btn-cart-link"]').click()
        return CartPage(self.driver)
