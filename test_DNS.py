from selenium import webdriver
from allure_commons.types import AttachmentType
import allure
import PageObject
import pytest


# Тесты для Tablet версии сайта DNS
class TestDNS:

    # Стартовые настройки тестов
    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.driver.set_window_size(900, 800)

    # При завершении всех квестов, закрыть драйвер
    def teardown_class(self):
        self.driver.close()

    # При запуске нового теста, перейти на главкную и поставить задержку
    def setup_method(self, method):
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.dns-shop.ru/")

    # Тест на соответствие результата поиска к запросу
    @allure.feature('Test search')
    @allure.story('Тестируем поиск сайта DNS в режиме Tablet')
    @allure.severity('critical')
    def test_search(self):
        # Делаем запись о шаге теста
        with allure.step('Инициализируем необходимые переменные'):
            # Запрос для поиска
            search_query = "Клавиатура"

        # Делаем запись о шаге теста
        with allure.step('Вводим в строку поиска: ' + search_query + '. И запускаем поиск'):
            # Передаем данные о драйвере главной странице
            main_page = PageObject.MainPage(self.driver)

            # Запускаем поиск
            search_result = main_page.search(search_query)

        # Делаем скриншот результатов поиска
        with allure.step('Cкриншот результатов поиска по запросу: ' + search_query):
            allure.attach(self.driver.get_screenshot_as_png(), name='SearchResult', attachment_type=AttachmentType.PNG)

        # Делаем запись о шаге теста
        with allure.step('Запускаем сравнение искомого значения с результатом поиска'):
            # Считываем названия найденных товаров и сравниваем их с запросом
            for elem in search_result.search_result_title():
                print("Res: " + elem.text)
                assert search_query.lower() in elem.text.lower()

    # Тест на правильность заполнения корзины
    @allure.feature('Test cart filling')
    @allure.story('Проверка заполнения корзины')
    @allure.severity('blocker')
    def test_cart(self):
        # Делаем запись о шаге теста
        with allure.step('Инициализируем необходимые переменные'):
            # Запрос для поиска
            search_query = "Клавиатура"

            # Номер найденого элемента, который нужно добавить в корзину
            product_id = 0

        # Делаем запись о шаге теста
        with allure.step('Вводим в строку поиска: ' + search_query + '. И запускаем поиск'):
            # Передаем данные о драйвере главной странице
            main_page = PageObject.MainPage(self.driver)

            # Запуск страницы поиска
            search_result = main_page.search(search_query)

        # Делаем запись о шаге теста
        with allure.step('Добавляем товар под номером ' + str(product_id + 1) + ' в корзину'):
            # Сохраняем название первого товара, так как добавим его в корзину
            product = search_result.search_result_title()[product_id].text

            # Добовление товара под номером product_id
            search_result.add_to_cart(product_id)

        # Делаем скриншот товара, добавленного в корзину
        with allure.step('Cкриншот товара, добавленного в корзину'):
            allure.attach(self.driver.get_screenshot_as_png(), name='SelectedItem', attachment_type=AttachmentType.PNG)

        # Делаем запись о шаге теста
        with allure.step('Открываем страницу корзины'):
            # Открываем корзину
            cart_page = main_page.open_cart()

        # Делаем скриншот страницы корзины
        with allure.step('Cкриншот страницы корзины'):
            allure.attach(self.driver.get_screenshot_as_png(), name='CartPage', attachment_type=AttachmentType.PNG)

        # Делаем запись о шаге теста
        with allure.step('Проверяем на правильность добавления товара в корзину'):
            # Получаем название продукта, нохоящегося в корзине и сравниваем с продуктом, который добовляли
            result_product = cart_page.name_product_in_cart()
            print("Res: " + result_product + " in " + product)
            assert result_product.lower() in product.lower()

    # Тест на правильно добавление товаров в сравнение
    @allure.feature('Comparison test')
    @allure.story('Проверка правильности добавления товаров в сравнение')
    @allure.severity('critical')
    def test_comparison(self):
        # Делаем запись о шаге теста
        with allure.step('Инициализируем необходимые переменные'):
            # Запрос для поиска
            search_query = "Клавиатура"

        # Делаем запись о шаге теста
        with allure.step('Вводим в строку поиска: ' + search_query + '. И запускаем поиск'):
            # Передаем данные о драйвере главной странице
            main_page = PageObject.MainPage(self.driver)

            # Запуск страницы поиска
            search_result = main_page.search(search_query)

        # Делаем запись о шаге теста
        with allure.step('Сохраняем результаты поиска в лист'):
            # Сохранение результата поиска
            comparison = []
            for elem in search_result.search_result_title():
                comparison.append(elem.text)

        # Делаем запись о шаге теста
        with allure.step('Выделяем необходимые элементы'):
            # Выбор элементов для 'Сравнение'
            search_result.search_result_checkbox()

        # Делаем скриншот элементов, добавляемых в сравнение
        with allure.step('Cкриншот элементов, добавляемых в сравнение'):
            allure.attach(self.driver.get_screenshot_as_png(), name='CompareItems', attachment_type=AttachmentType.PNG)

        # Делаем запись о шаге теста
        with allure.step('Переходим на страницу "Сравнение"'):
            # Переход к страницы 'Сравнение'
            page_comparison = main_page.start_comparison()

        # Делаем скриншот вкладки сравнения
        with allure.step('Cкриншот вкладки сравнения'):
            allure.attach(self.driver.get_screenshot_as_png(), name='ComparisonPage', attachment_type=AttachmentType.PNG)

        # Делаем запись о шаге теста
        with allure.step('Проверяем на правильность добавления товаров в сравнение'):
            # Получение названий таваров на странице 'Сравнение' и проверка на равенство выбраным объектам
            comparison_result = page_comparison.search_product_in_comparison()
            for i in range(2):
                print("Res: " + str(comparison[i]) + " in " + comparison_result[i].text)
                assert comparison[i].lower() in comparison_result[i].text.lower()
